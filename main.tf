locals {
  instances = [for instance in var.resources : {
    name                        = instance.name
    instance_type               = instance.instance_type
    ami                         = instance.ami
    subnet_id                   = try(instance.subnet_id != null ? instance.subnet_id : null, null)
    associate_public_ip_address = try(instance.associate_public_ip_address != null ? instance.associate_public_ip_address : null, null)
    user_data_base64            = try(instance.user_data_base64 != null ? instance.user_data_base64 : null, null)
    user_data_replace_on_change = try(instance.user_data_replace_on_change != null ? instance.user_data_replace_on_change : null, null)
    tags                        = try(instance.tags != null ? instance.tags : null, null)
  }]

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}

resource "aws_instance" "this" {
  for_each = { for instance in local.instances : instance.name => instance }

  ami                         = each.value.ami
  instance_type               = each.value.instance_type
  subnet_id                   = each.value.subnet_id
  user_data_replace_on_change = each.value.user_data_replace_on_change
  user_data_base64            = each.value.user_data_base64
  associate_public_ip_address = each.value.associate_public_ip_address

  tags = merge(local.common_tags, each.value.tags != null ? each.value.tags : {
    Name        = each.value.name
    Description = "Created by ${var.project.createdBy}"
  })
}
