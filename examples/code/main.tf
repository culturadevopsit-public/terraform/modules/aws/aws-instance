provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_from_code" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = [
    {
      name                        = "web-1"
      instance_type               = "t2.micro"
      ami                         = "ami-xxxxx"
      user_data_replace_on_change = true
      user_data_base64            = "ZWNobyAiXG5DdWx0dXJhRGV2b3AgLSBtQXJraXRvc1xuIg=="
      associate_public_ip_address = true
      tags = {
        Name = "web-1"
      }
    },
    {
      name          = "web-2"
      instance_type = "t3.micro"
      ami           = "ami-xxxxx"
    }
  ]
}
