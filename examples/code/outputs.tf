output "instances_ids" {
  description = "Ids of the created EC2 instances."
  value       = module.module_usage_from_code.instances_ids
}

output "instances_arns" {
  description = "Arns of the created EC2 instances."
  value       = module.module_usage_from_code.instances_arns
}

output "instance_public_ips" {
  description = "List of public IP addresses assigned to the instances, if applicable."
  value       = module.module_usage_from_code.public_ips
}
